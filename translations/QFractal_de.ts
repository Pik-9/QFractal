<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutWin</name>
    <message>
        <location filename="../src/AboutWin.cpp" line="57"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>ColorWindow</name>
    <message>
        <location filename="../src/ColorWin.cpp" line="68"/>
        <source>Simple color theme</source>
        <translation>Einfaches Farbschema</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="70"/>
        <source>Just six simple colors.</source>
        <translation>Einfach nur die sechs Grundfarben.</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="74"/>
        <source>Rainbow color theme</source>
        <translation>Regenbogenfarbenschema</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="76"/>
        <source>Rotate through the colors of the rainbow.</source>
        <translation>Rotiere über den Farbkreis.</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="80"/>
        <source>Fading color theme</source>
        <translation>Farbverlaufsschema</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="82"/>
        <source>Choose color</source>
        <translation>Farbe auswählen</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="89"/>
        <source>Gradient from color 1 to 2</source>
        <translation>Farbverlauf von Farbe 1 zu 2</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="91"/>
        <source>The 1st color</source>
        <translation>Die erste Farbe</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="92"/>
        <source>The 2nd color</source>
        <translation>Die zweite Farbe</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="103"/>
        <source>Load map file</source>
        <translation>Farben aus Datei laden</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="105"/>
        <source>Load colors from file:</source>
        <translation>Farben aus Datei laden:</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="107"/>
        <source>Search...</source>
        <translation>Suchen...</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="108"/>
        <source>Load MAP file</source>
        <translation>Lade Farbdatei</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="108"/>
        <source>All (*)</source>
        <translation>Alles (*)</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="109"/>
        <source>&lt;MAP file&gt;</source>
        <translation>&lt;Farbdatei&gt;</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="117"/>
        <source>Simple colors</source>
        <translation>Grundfarben</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="118"/>
        <source>Rainbow colors</source>
        <translation>Regenbogenfarben</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="119"/>
        <source>Fading color</source>
        <translation>Einfarbiger Farbverlauf</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="120"/>
        <source>Gradient</source>
        <translation>Farbverlauf</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="121"/>
        <source>Map file</source>
        <translation>Farbdatei</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="132"/>
        <source>Apply</source>
        <translation>Übernehmen</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="133"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="147"/>
        <source>Choose color theme</source>
        <translation>Wähle Farbschema</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="180"/>
        <source>Bad file</source>
        <translation>Dateifehler</translation>
    </message>
    <message>
        <location filename="../src/ColorWin.cpp" line="181"/>
        <source>Error: the file doesn&apos;t exist. Using previous color sheme.</source>
        <translation>Fehler: Die Datei existiert nicht. Benutze vorheriges Farbschema.</translation>
    </message>
</context>
<context>
    <name>QFractal</name>
    <message>
        <location filename="../src/QFractal.cpp" line="38"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="39"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="42"/>
        <source>Select color theme</source>
        <translation>Wähle Farbschema</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="43"/>
        <source>Export image</source>
        <translation>Exportiere Bilddatei</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="44"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="45"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="54"/>
        <source>Control</source>
        <translation>Kontrolle</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="62"/>
        <source>Mandelbrot</source>
        <translation>Mandelbrot</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="63"/>
        <source>Julia</source>
        <translation>Julia</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="64"/>
        <source>Julia parameter c</source>
        <translation>Julia c Parameter</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="66"/>
        <source>Re(c)</source>
        <translation>Re(c)</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="67"/>
        <source>Im(c)</source>
        <translation>Im(c)</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="70"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="71"/>
        <source>Bottom</source>
        <translation>Unten</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="72"/>
        <source>Width</source>
        <translation>Breite</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="73"/>
        <source>Height</source>
        <translation>Höhe</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="74"/>
        <source>Max. iterations</source>
        <translation>Max. Iterationen</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="80"/>
        <source>Reset zoom</source>
        <translation>Setze Ausschnitt zurück</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="81"/>
        <source>Calculate</source>
        <translation>Berechne</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="135"/>
        <location filename="../src/QFractal.cpp" line="172"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="165"/>
        <source>Calculating...  Be patient, please.</source>
        <translation>Berechne... Bitte warten.</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="212"/>
        <source>Save as JPEG...</source>
        <translation>Speichern als JPEG...</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="213"/>
        <source>JPEG-Files (*.jpg *.jpeg)</source>
        <translation>JPEG-Dateien (*.jpg *.jpeg)</translation>
    </message>
    <message>
        <location filename="../src/QFractal.cpp" line="220"/>
        <source>Saved fractal to file.</source>
        <translation>Fraktal in Datei gespeichert.</translation>
    </message>
</context>
</TS>
