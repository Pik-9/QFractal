# QFractal - A fractal drawing program

Copyright (C) 2015-2021 Daniel Steinhauer

This application provides a Qt powered GUI where
Julia and Mandelbrot fractals can be calculated and
drawn with several different color themes.

The calculation is optimized for multicore CPUs by using
OpenMP.

## Color maps
You can also load your own color map from a textfile
that looks like this:
  #Red #Green #Blue
  255 0 0
  128 255 0
  ...

## Building
```bash
cd /path/to/QFractal
cmake .
make
```
