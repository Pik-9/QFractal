/************************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.              *
 *                                                                      *
 * (C) 2021 Daniel Steinhauer <d.steinhauer@mailbox.org>                *
 *                                                                      *
 * QFractal is free software; you can redistribute it and/or modify     *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * QFractal is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with QFractal; if not, write to the Free Software Foundation,  *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA    *
 ************************************************************************/

 /**
 * @file This file contains the functions definitions from fractal canvas
 * widget.
 */

#include "FracWidget.hpp"
#include "ColorSet.hpp"

#include <QPainter>

FracCanvas::FracCanvas ()
  : QWidget (), iterMatrix (NULL), cs (NULL),
  rsel (new QRubberBand (QRubberBand::Rectangle, this)),
  zoomReady (false)
{}

void FracCanvas::setReadyToZoom (const bool ready2zoom)
{
  zoomReady = ready2zoom;
}

void FracCanvas::mousePressEvent (QMouseEvent *event)
{
  if (zoomReady)  {
    //Set the first point of selected area.
    pos1 = event->pos ();
    rsel->setGeometry (QRect (pos1, QSize ()));
    rsel->show ();
  }
}

void FracCanvas::mouseMoveEvent (QMouseEvent *event)
{
  if (zoomReady)  {
    //Resize the selected area rectangle to current cursor position.
    rsel->setGeometry (QRect (pos1, event->pos ()).normalized ());
  }
}

void FracCanvas::mouseReleaseEvent (QMouseEvent *event)
{
  if (zoomReady)  {
    //Area selected
    rsel->hide ();
    
    //Ensure that user really selected an area and didn't just click in the widget.
    if ((rsel->geometry ().width () > 0) && (rsel->geometry ().height () > 0))  {
      emit (selectArea (rsel->geometry ()));
    }
  }
}

void FracCanvas::resizeEvent (QResizeEvent *event)
{
  setMatrix (NULL);
}

void FracCanvas::setMatrix (unsigned int *nMatrix)
{
  //If we already have a matrix, remove it properly from memory before.
  if (iterMatrix)  {
    delete[] iterMatrix;
  }
  iterMatrix = nMatrix;
}

void FracCanvas::setColorSet (ColorSet *colors)
{
  //If we already have a color theme, remove it properly from memory before.
  if (cs)  {
    delete cs;
  }
  cs = colors;
}

void FracCanvas::paintEvent (QPaintEvent *event)
{
  QPainter painter (this);
  if (iterMatrix)  {
    //Draw the fractal from iterMatrix
    for (register uint iy = 0; iy < this->height (); ++iy)  {
      for (register uint ix = 0; ix < this->width (); ++ix)  {
        painter.setPen (cs->getColor (iterMatrix[iy * width () + ix]));
        painter.drawPoint(ix, iy);
      }
    }
  } else  {
    //If we don't have a matrix to draw yet, just paint it black.
    painter.fillRect (this->rect (), Qt::black);
  }
  painter.end ();
}
