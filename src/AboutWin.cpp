/************************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.              *
 *                                                                      *
 * (C) 2021 Daniel Steinhauer <d.steinhauer@mailbox.org>                *
 *                                                                      *
 * QFractal is free software; you can redistribute it and/or modify     *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * QFractal is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with QFractal; if not, write to the Free Software Foundation,  *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA    *
 ************************************************************************/

/**
 * @file This file contains the about window's function definitions.
 */

#include "AboutWin.hpp"

/**
 * @param README Just the beginning of the README file.
 * This will be listed in the textedit.
 */
const char* AboutWin::README = {
  "QFractal - A fractal drawing program\n"
  "====================================\n"
  "\n"
  "Author: Daniel Steinhauer\n"
  "\n"
  "This application provides a Qt powered GUI where\n"
  "Julia and Mandelbrot fractals can be calculated and\n"
  "drawn with several different color themes.\n"
  "\n"
  "You can also load your own color map from a textfile\n"
  "that looks like this:\n"
  "  #Red #Green #Blue\n"
  "  255 0 0\n"
  "  128 255 0\n"
  "  ...\n"
  "\n"
  "The calculation is optimized for multicore CPUs by using\n"
  "OpenMP."
};

AboutWin::AboutWin ()
  : CenteredWidget (500, 500)
{
  l = new QVBoxLayout (this);
  t = new QTextEdit ();
  b = new QPushButton (tr ("Ok"));
  b->setAutoDefault (true);
  t->setReadOnly (true);
  t->setText (QString (README));

  l->addWidget (t);
  l->addWidget (b);

  connect (b, SIGNAL (clicked ()), this, SLOT (hide ()));
}
