/*********************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.           *
 *                                                                   *
 * (C) 2014 Daniel Steinhauer                                        *
 *                                                                   *
 * This file is part of QFractal.                                    *
 *********************************************************************
 * This file contains a basement class for all fractals and the      *
 * Julia and Mandelbrot fractals derived from it.                    *
 *********************************************************************/

#ifndef FRACTAL_H
#define FRACTAL_H

/*
 * The basement class for all fractals.
 */
class CFractal
{
public:
  static unsigned int max_iter;
  
  CFractal ();
  virtual ~CFractal ();
  
  virtual unsigned int calcIter (const double, const double)=0;
};

class CMandelbrot : public CFractal
{
public:
  CMandelbrot ();
  ~CMandelbrot ();
  
  /*
   * Calculate the iterations of Mandelbrot fractal for
   * z = x + i*y
   */
  unsigned int calcIter (const double, const double);
};

class CJulia : public CFractal
{
private:
  double c_x, c_y;
  
public:
  CJulia (const double, const double);
  ~CJulia ();
  
  /*
   * Calculate the iterations of Julia fractal for
   * z = x + i*y
   */
  unsigned int calcIter (const double, const double);
};

#endif