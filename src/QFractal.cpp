/************************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.              *
 *                                                                      *
 * (C) 2021 Daniel Steinhauer <d.steinhauer@mailbox.org>                *
 *                                                                      *
 * QFractal is free software; you can redistribute it and/or modify     *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * QFractal is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with QFractal; if not, write to the Free Software Foundation,  *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA    *
 ************************************************************************/

/**
 * @file This file contains the main window's function definitions.
 */

#include "QFractal.hpp"

#include <QStatusBar>
#include <QMenuBar>
#include <QApplication>
#include <QFileDialog>
#include <QPixmap>

QFractal::QFractal ()
{
  can = new FracCanvas ();
  cw = new ColorWindow (can);
  aw = new AboutWin ();
  fileMenu = new QMenu (tr ("File"));
  helpMenu = new QMenu (tr ("Help"));
  menuBar ()->addMenu (fileMenu);
  menuBar ()->addMenu (helpMenu);
  fileMenu->addAction (tr ("Select color theme"), cw, SLOT (show ()));
  fileMenu->addAction (tr ("Export image"), this, SLOT (saveJPEG ()));
  fileMenu->addAction (tr ("Quit"), qApp, SLOT (quit ()));
  helpMenu->addAction (tr ("About"), aw, SLOT (show ()));
  
  worker = new Calculator ();
  
  //Create an unvisible GroupBox to put all the stuff in
  mainb = new QGroupBox ();
  mainb->setFlat (true);
  mainl = new QGridLayout (mainb);
  
  ctrlb = new QGroupBox (tr ("Control"));
  ctrll = new QGridLayout (ctrlb);
  mainl->addWidget (can, 0, 0, 1, 4);
  mainl->addWidget (ctrlb, 0, 4, 1, 1);
  
  //Set the default color sheme of ColorWindow
  cw->clickOk ();
  
  rmandel = new QRadioButton (tr ("Mandelbrot"));
  rjulia = new QRadioButton (tr ("Julia"));
  juliab = new QGroupBox (tr ("Julia parameter c"));
  julial = new QGridLayout (juliab);
  ccx = new QLabel (tr ("Re(c)"));
  ccy = new QLabel (tr ("Im(c)"));
  scx = new QDoubleSpinBox ();
  scy = new QDoubleSpinBox ();
  cleft = new QLabel (tr ("Left"));
  cbottom = new QLabel (tr ("Bottom"));
  cwidth = new QLabel (tr ("Width"));
  cheight = new QLabel (tr ("Height"));
  citer = new QLabel (tr ("Max. iterations"));
  sleft = new QDoubleSpinBox ();
  sbottom = new QDoubleSpinBox ();
  swidth = new QDoubleSpinBox ();
  sheight = new QDoubleSpinBox ();
  siter = new QSpinBox ();
  zOut = new QPushButton (tr ("Reset zoom"));
  act = new QPushButton (tr ("Calculate"));
  
  rmandel->setChecked (true);
  scx->setValue (0.35);
  scy->setValue (0.35);
  scx->setRange (-10.0, 10.0);
  scy->setRange (-10.0, 10.0);
  scx->setDecimals (5);
  scy->setDecimals (5);
  scx->setEnabled (false);
  scy->setEnabled (false);
  sleft->setRange (-2.0, 2.0);
  sleft->setValue (-2.0);
  sbottom->setRange (-2.0, 2.0);
  sbottom->setValue (-2.0);
  swidth->setRange (0.0, 4.0);
  swidth->setValue (4.0);
  sheight->setValue (4.0);
  sheight->setRange (0.0, 4.0);
  siter->setValue (16);
  siter->setMaximum (99999);
  
  sleft->setDecimals (5);
  sbottom->setDecimals (5);
  swidth->setDecimals (5);
  sheight->setDecimals (5);
  
  julial->addWidget (ccx, 0, 0, 1, 1);
  julial->addWidget (scx, 0, 1, 1, 1);
  julial->addWidget (ccy, 1, 0, 1, 1);
  julial->addWidget (scy, 1, 1, 1, 1);
  
  ctrll->addWidget (rmandel, 0, 0, 1, 2);
  ctrll->addWidget (rjulia, 1, 0, 1, 2);
  ctrll->addWidget (juliab, 2, 0, 1, 2);
  ctrll->addWidget (cleft, 3, 0, 1, 1);
  ctrll->addWidget (sleft, 3, 1, 1, 1);
  ctrll->addWidget (cbottom, 4, 0, 1, 1);
  ctrll->addWidget (sbottom, 4, 1, 1, 1);
  ctrll->addWidget (cwidth, 5, 0, 1, 1);
  ctrll->addWidget (swidth, 5, 1, 1, 1);
  ctrll->addWidget (cheight, 6, 0, 1, 1);
  ctrll->addWidget (sheight, 6, 1, 1, 1);
  ctrll->addWidget (citer, 7, 0, 1, 1);
  ctrll->addWidget (siter, 7, 1, 1, 1);
  ctrll->addWidget (zOut, 8, 0, 1, 2);
  ctrll->addWidget (act, 9, 0, 1, 2);
  
  connect (act, SIGNAL (clicked ()), this, SLOT (actOnClick ()));
  connect (zOut, SIGNAL (clicked ()), this, SLOT(zoomOut ()));
  connect (can, SIGNAL (selectArea(QRect)), this, SLOT (zoomIn (QRect)));
  connect (rjulia, SIGNAL (toggled (bool)), this, SLOT (selectJulia (bool)));
  connect (worker, SIGNAL (giveMatrix (uint*)), this, SLOT (gotMatrix (uint*)));
  
  statusBar ()->showMessage (tr ("Ready"));
  
  this->setCentralWidget (mainb);
  this->setWindowTitle ("QFractal");
  this->showMaximized ();
}

QFractal::~QFractal ()
{}

void QFractal::actOnClick ()
{
  //Disable calculation commands while calculating
  can->setReadyToZoom (false);
  act->setEnabled (false);
  zOut->setEnabled (false);
  
  //Surrender values for calculation
  CFractal::max_iter = siter->value ();
  CFractal *fra;
  if (rmandel->isChecked ())  {
    fra = new CMandelbrot ();
  } else  {
    fra = new CJulia (scx->value (), scy->value ());
  }
  worker->setValues (sleft->value (), sbottom->value (),
                     swidth->value (), sheight->value (),
                     can->width (), can->height (), fra);
  
  worker->start ();
  statusBar ()->showMessage (tr ("Calculating...  Be patient, please."));
}

void QFractal::gotMatrix (unsigned int *matrix)
{
  can->setMatrix (matrix);
  can->repaint ();
  statusBar ()->showMessage (tr ("Ready"));
  
  //Enable the buttons, which start a calculation again.
  act->setEnabled (true);
  zOut->setEnabled (true);
  can->setReadyToZoom (true);
}

void QFractal::selectJulia (bool checked)
{
  scx->setEnabled (checked);
  scy->setEnabled (checked);
}

void QFractal::zoomIn (QRect area)
{
  //Set the new values for the zoom
  sleft->setValue (sleft->value () + (swidth->value () * area.left ()) / (double) can->width ());
  sbottom->setValue (sbottom->value () + (sheight->value () * area.top ()) / (double) can->height ());
  swidth->setValue ((area.width () * swidth->value ()) / (double) can->width ());
  sheight->setValue ((area.height () * sheight->value ()) / (double) can->height ());
  
  //Draw the new fractal
  actOnClick ();
}

void QFractal::zoomOut ()
{
  sleft->setValue (-2.0);
  sbottom->setValue (-2.0);
  swidth->setValue (4.0);
  sheight->setValue (4.0);
  
  //Redraw the fractal
  actOnClick ();
}

void QFractal::saveJPEG ()
{
  QString imgPath = QFileDialog::getSaveFileName (
    this, tr ("Save as JPEG..."),
    QDir::homePath (), tr ("JPEG-Files (*.jpg *.jpeg)")
  );
  
  //Check whether the user really entered a file name or pressed cancel.
  if (!imgPath.isEmpty ())  {
    QPixmap out = can->grab ();
    out.save (imgPath, "JPG", 50);
    statusBar ()->showMessage (tr ("Saved fractal to file."));
  }
}
