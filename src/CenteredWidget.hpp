/*********************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.           *
 *                                                                   *
 * (C) 2021 Daniel Steinhauer                                        *
 *                                                                   *
 * This file is part of QFractal.                                    *
 *********************************************************************
 * This file contains a basic centered dialog window.                *
 *********************************************************************/

#ifndef CENTERED_WIDGET_H
#define CENTERED_WIDGET_H

#include <QWidget>

class CenteredWidget : public QWidget
{
public:
  CenteredWidget (const unsigned int, const unsigned int);
  virtual ~CenteredWidget ();
};

#endif
