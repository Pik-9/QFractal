/************************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.              *
 *                                                                      *
 * (C) 2021 Daniel Steinhauer <d.steinhauer@mailbox.org>                *
 *                                                                      *
 * QFractal is free software; you can redistribute it and/or modify     *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * QFractal is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with QFractal; if not, write to the Free Software Foundation,  *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA    *
 ************************************************************************/

 /**
 * @file This file contains the main function, which starts the Qt
 * event loop.
 */

#include <QApplication>
#include <QTranslator>
#include <QLocale>

#include "QFractal.hpp"

int main (int argc, char *argv[])
{
  QApplication app (argc, argv);
  QTranslator *trans = new QTranslator ();
  trans->load (QString ("QFractal_%1").arg (QLocale::system ().name ()), "/usr/local/share/QFractal");
  app.installTranslator (trans);
  QFractal qfractal;
  return app.exec ();
}
