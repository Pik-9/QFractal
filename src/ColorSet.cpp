/************************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.              *
 *                                                                      *
 * (C) 2021 Daniel Steinhauer <d.steinhauer@mailbox.org>                *
 *                                                                      *
 * QFractal is free software; you can redistribute it and/or modify     *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * QFractal is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with QFractal; if not, write to the Free Software Foundation,  *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA    *
 ************************************************************************/

 /**
 * @file This file contains the function definitions of all color theme
 * classes.
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * Unfortunately this definition is necessary to access the mathematical
 * constants on some platforms.
 */
#define _USE_MATH_DEFINES
#include <math.h>

#include "ColorSet.hpp"
#include "Fractal.hpp"

const QColor SimpleColor::basic_set[6] = {
  Qt::red,
  Qt::green,
  Qt::blue,
  Qt::magenta,
  Qt::yellow,
  Qt::cyan
};

ColorSet::ColorSet ()
{}

ColorSet::~ColorSet ()
{}

SimpleColor::SimpleColor ()
{}

SimpleColor::~SimpleColor ()
{}

QColor SimpleColor::getColor (const unsigned int iter)
{
  return basic_set[iter % 6];
}

FadingLightning::FadingLightning (const QColor end)
  : endcolor (end)
{}

FadingLightning::~FadingLightning ()
{}

QColor FadingLightning::getColor (const unsigned int iter)
{
  QColor RET;
  if (iter == CFractal::max_iter)  {
    RET = Qt::black;
  } else  {
    //Assuming linear gradients for colors like c(iter) = m * iter
    double m_red = endcolor.red () / (double) (CFractal::max_iter);
    double m_green = endcolor.green () / (double) (CFractal::max_iter);
    double m_blue = endcolor.blue () / (double) (CFractal::max_iter);
    
    RET.setRed (m_red * (iter));
    RET.setGreen (m_green * (iter));
    RET.setBlue (m_blue * (iter));
  }
  return RET;
}

GradientColor::GradientColor (const QColor color_1, const QColor color_2)
  : col1 (color_1), col2 (color_2)
{}

GradientColor::~GradientColor ()
{}

QColor GradientColor::getColor (const unsigned int iter)
{
  //Assuming linear gradients for color like c(iter) = m * iter + col1
  double m_red = (double) (col2.red () - col1.red ()) / (double) CFractal::max_iter;
  double m_green = (double) (col2.green () - col1.green ()) / (double) CFractal::max_iter;
  double m_blue = (double) (col2.blue () - col1.blue ()) / (double) CFractal::max_iter;
  
  QColor RET;
  RET.setRed (iter * m_red + col1.red ());
  RET.setGreen (iter * m_green + col1.green ());
  RET.setBlue (iter * m_blue + col1.blue ());
  return RET;
}

Filemap::Filemap (const char *filepath)
{
  FILE *inFile = fopen (filepath, "r");
  map = NULL;
  mapSize = 0;
  while (!feof (inFile))  {
    map = (QColor*) realloc ((void*) map, ++mapSize * sizeof (QColor));
    unsigned int red, green, blue;
    fscanf (inFile, "%u %u %u\n", &red, &green, &blue);
    map[mapSize - 1] = QColor (red, green, blue);
  }
  fclose (inFile);
}

Filemap::~Filemap ()
{
  delete[] map;
}

QColor Filemap::getColor (const unsigned int iter)
{
  return map[iter % mapSize];
}

const double RainbowColors::phi_red = 2.0 * M_PI / 3.0;
const double RainbowColors::phi_green = -2.0 * M_PI / 3.0;
const double RainbowColors::phi_blue = 0.0;


RainbowColors::RainbowColors ()
{}

RainbowColors::~RainbowColors ()
{}

QColor RainbowColors::getColor (const unsigned int iter)
{
  double phi = 2.0 * M_PI * (double) iter / (double) CFractal::max_iter;
  
  QColor RET;
  RET.setRed (floor (127.5 * (1.0 + cos (phi - phi_red))));
  RET.setGreen (floor (127.5 * (1.0 + cos (phi - phi_green))));
  RET.setBlue (floor (127.5 * (1.0 + cos (phi - phi_blue))));
  
  return RET;
}
