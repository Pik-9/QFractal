/************************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.              *
 *                                                                      *
 * (C) 2021 Daniel Steinhauer <d.steinhauer@mailbox.org>                *
 *                                                                      *
 * QFractal is free software; you can redistribute it and/or modify     *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * QFractal is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with QFractal; if not, write to the Free Software Foundation,  *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA    *
 ************************************************************************/

/**
 * @file This file contains the color window's function definitions.
 */

#include "ColorWin.hpp"
#include "Fractal.hpp"

#include <QPainter>
#include <QMessageBox>

ColorButton::ColorButton ()
  : QPushButton (), color (Qt::white), chooser (new QColorDialog ())
{
  connect (this, SIGNAL (clicked ()), chooser, SLOT (open ()));
  connect (chooser, SIGNAL (colorSelected (QColor)), this, SLOT (setColor (QColor)));
}

void ColorButton::setColor (QColor nCol)
{
  color = nCol;
  repaint ();
}

QColor ColorButton::getColor ()
{
  return color;
}

void ColorButton::paintEvent (QPaintEvent *event)
{
  QPushButton::paintEvent (event);
  QPainter painter (this);
  QRect geom = rect ();
  QRect cen;
  cen.setWidth (0.7 * geom.width ());
  cen.setHeight (0.5 * geom.height ());
  cen.moveCenter (geom.center ());
  painter.fillRect (cen, color);
  painter.end ();
}

//--------------------------------------------------------------------

ColorWindow::ColorWindow (FracCanvas *screen)
  : CenteredWidget (700, 200), canvas (screen)
{
  //Create the simple color layout
  gsimple = new QGroupBox (tr ("Simple color theme"));
  lsimple = new QHBoxLayout (gsimple);
  simple_label = new QLabel (tr ("Just six simple colors."));
  lsimple->addWidget (simple_label);
  
  //Create the rainbow color layout
  grainbow = new QGroupBox (tr ("Rainbow color theme"));
  lrainbow = new QHBoxLayout (grainbow);
  rainbow_label = new QLabel (tr ("Rotate through the colors of the rainbow."));
  lrainbow->addWidget (rainbow_label);
  
  //Create the fading color layout
  gfading = new QGroupBox (tr ("Fading color theme"));
  lfading = new QGridLayout (gfading);
  fading_label = new QLabel (tr ("Choose color"));
  fading_button = new ColorButton ();
  fading_button->setColor (QColor (80, 0, 255));
  lfading->addWidget (fading_label, 0, 0, 1, 3);
  lfading->addWidget (fading_button, 1, 1, 1, 1);
  
  //Create the gradient color layout
  ggrad = new QGroupBox (tr ("Gradient from color 1 to 2"));
  lgrad = new QGridLayout (ggrad);
  lcol1 = new QLabel (tr ("The 1st color"));
  lcol2 = new QLabel (tr ("The 2nd color"));
  bcol1 = new ColorButton ();
  bcol2 = new ColorButton ();
  bcol1->setColor (Qt::red);
  bcol2->setColor (Qt::cyan);
  lgrad->addWidget (lcol1, 0, 0, 1, 1);
  lgrad->addWidget (lcol2, 0, 1, 1, 1);
  lgrad->addWidget (bcol1, 1, 0, 1, 1);
  lgrad->addWidget (bcol2, 1, 1, 1, 1);
  
  //Create the map file layout
  gfile = new QGroupBox (tr ("Load map file"));
  lfile = new QGridLayout (gfile);
  cfile = new QLabel (tr ("Load colors from file:"));
  tfile = new QLineEdit ();
  bfile = new QPushButton (tr ("Search..."));
  file_dia = new QFileDialog (this, tr ("Load MAP file"), QDir::homePath (), tr ("All (*)"));
  tfile->setText (tr ("<MAP file>"));
  tfile->setReadOnly (true);
  lfile->addWidget (cfile, 0, 0, 1, 5);
  lfile->addWidget (tfile, 1, 0, 1, 4);
  lfile->addWidget (bfile, 1, 4, 1, 1);
  
  //Create the widget stack
  cslist = new QListWidget ();
  cslist->addItem (tr ("Simple colors"));
  cslist->addItem (tr ("Rainbow colors"));
  cslist->addItem (tr ("Fading color"));
  cslist->addItem (tr ("Gradient"));
  cslist->addItem (tr ("Map file"));
  cslist->setCurrentRow (2);
  csdias = new QStackedLayout ();
  csdias->addWidget (gsimple);
  csdias->addWidget (grainbow);
  csdias->addWidget (gfading);
  csdias->addWidget (ggrad);
  csdias->addWidget (gfile);
  csdias->setCurrentIndex (2);
  
  //Create the main layout
  bok = new QPushButton (tr ("Apply"));
  bcan = new QPushButton (tr ("Cancel"));
  lmain = new QGridLayout (this);
  lmain->addWidget (cslist, 0, 0, 1, 3);
  lmain->addLayout (csdias, 0, 3, 1, 3);
  lmain->addWidget (bcan, 1, 2, 1, 1);
  lmain->addWidget (bok, 1, 3, 1, 1);
  
  connect (cslist, SIGNAL (currentRowChanged (int)), csdias, SLOT (setCurrentIndex (int)));
  connect (bok, SIGNAL (clicked ()), this, SLOT (clickOk ()));
  connect (bcan, SIGNAL (clicked ()), this, SLOT (hide ()));
  
  connect (bfile, SIGNAL (clicked ()), file_dia, SLOT (exec ()));
  connect (file_dia, SIGNAL (fileSelected (QString)), tfile, SLOT (setText (QString)));

  this->setWindowTitle (tr ("Choose color theme"));
}

void ColorWindow::clickOk ()
{
  switch (cslist->currentRow ())  {
    case 0:  {
      canvas->setColorSet (new SimpleColor ());
      break;
    }
    
    case 1:  {
      canvas->setColorSet (new RainbowColors ());
      break;
    }
    
    case 2:  {
      canvas->setColorSet (new FadingLightning (fading_button->getColor ()));
      break;
    }
    
    case 3:  {
      canvas->setColorSet (new GradientColor (bcol1->getColor (), bcol2->getColor ()));
      break;
    }
    
    case 4:  {
      QFile test (tfile->text ());
      if (test.exists ())  {
        canvas->setColorSet (new Filemap (tfile->text ().toStdString ().c_str ()));
      } else  {
        QMessageBox::warning (
          this,
          tr ("Bad file"),
          tr ("Error: the file doesn't exist. Using previous color sheme.")
        );
      }
      break;
    }
  }
  
  this->hide ();
}
