/*********************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.           *
 *                                                                   *
 * (C) 2014 Daniel Steinhauer                                        *
 *                                                                   *
 * This file is part of QFractal.                                    *
 *********************************************************************
 * This file contains the programs main window and its functions.    *
 *********************************************************************/

#ifndef QFractal_H
#define QFractal_H

#include <QMainWindow>
#include <QGroupBox>
#include <QGridLayout>
#include <QRadioButton>
#include <QLabel>
#include <QPushButton>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QMenu>

#include "FracWidget.hpp"
#include "FracCalc.hpp"
#include "ColorWin.hpp"
#include "AboutWin.hpp"

class QFractal : public QMainWindow
{
  Q_OBJECT
private:
  Calculator *worker;
  ColorWindow *cw;
  AboutWin *aw;
  
  QMenu *fileMenu, *helpMenu;
  QGroupBox *mainb, *ctrlb;
  QGridLayout *mainl, *ctrll;
  FracCanvas *can;
  
  //Ctrl box
  QRadioButton *rmandel, *rjulia;
  QLabel *cleft, *cbottom, *cwidth, *cheight, *citer;
  QDoubleSpinBox *sleft, *sbottom, *swidth, *sheight;
  QSpinBox *siter;
  QPushButton *act, *zOut;
  
  //Julia c box
  QGroupBox *juliab;
  QGridLayout *julial;
  QLabel *ccx, *ccy;
  QDoubleSpinBox *scx, *scy;

public:
  QFractal ();
  virtual ~QFractal ();
  
public slots:
  //Start calculation of the fractal.
  void actOnClick ();
  
  //Receive calculated fractal as a matrix and draw it.
  void gotMatrix (unsigned int*);
  
  //The user toggled to draw a Julia fractal.
  void selectJulia (bool);
  
  //The user wants to zoom into an area.
  void zoomIn (QRect);
  
  //Reset the zoom to [-2:2][-2:2].
  void zoomOut ();
  
  //Save the fractal to a JPEG file.
  void saveJPEG ();
};

#endif // QFractal_H
