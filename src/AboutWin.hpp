/************************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.              *
 *                                                                      *
 * (C) 2021 Daniel Steinhauer <d.steinhauer@mailbox.org>                *
 *                                                                      *
 * QFractal is free software; you can redistribute it and/or modify     *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * QFractal is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with QFractal; if not, write to the Free Software Foundation,  *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA    *
 ************************************************************************/

/**
 * @file This file contains the window to show a short program info and
 * its function declarations.
 */

#ifndef ABOUT_WIN_H
#define ABOUT_WIN_H

#include "CenteredWidget.hpp"

#include <QVBoxLayout>
#include <QTextEdit>
#include <QPushButton>

class AboutWin : public CenteredWidget
{
  Q_OBJECT
private:
  QVBoxLayout *l;
  QTextEdit *t;
  QPushButton *b;
  
  static const char *README;
  
public:
  AboutWin ();
};

#endif
