/*********************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.           *
 *                                                                   *
 * (C) 2014 Daniel Steinhauer                                        *
 *                                                                   *
 * This file is part of QFractal.                                    *
 *********************************************************************
 * This file contains the canvas (derived from QWidget) where the    *
 * fractals are drawn on.                                            *
 *********************************************************************/

#ifndef FRACW_H
#define FRACW_H

#include <QWidget>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QRubberBand>

#include "ColorSet.hpp"

/*
 * This is the canvas to draw the fractal on.
 */
class FracCanvas : public QWidget
{
  Q_OBJECT

private:
  //The color theme for the fractal.
  ColorSet *cs;
  
  //The rectangle to handle the rect. area selection.
  QRubberBand *rsel;
  
  //The first point of the selected rectangle.
  QPoint pos1;
  
  /* 
   * Is able to zoom in now?
   * Every zoom starts a new calculation. If we start zooming while
   * the program is calculating, it might crash! Therefor we have to
   * disable it during calculation.
   */
  bool zoomReady;
  
public:
  //The fractal matrix to draw.
  unsigned int *iterMatrix;
  
public:
  FracCanvas ();
  
  void setReadyToZoom (const bool);
  
  //Handle the events to select an area to zoom in.
  void mousePressEvent (QMouseEvent*);
  void mouseMoveEvent (QMouseEvent*);
  void mouseReleaseEvent (QMouseEvent*);
  
  /* 
   * Ensure that the iterMatrix is deleted when
   * the window resizes.
   * Otherwise the program might crash, because
   * the matrix is fix for a specific size!
   */
  void resizeEvent (QResizeEvent*);
  
  void setMatrix (unsigned int*);
  void setColorSet (ColorSet*);
  
  //The interesting stuff!
  void paintEvent (QPaintEvent*);
  
signals:
  //Emit when user selected an area to zoom in.
  void selectArea (QRect area);
};

#endif