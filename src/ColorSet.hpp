/*********************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.           *
 *                                                                   *
 * (C) 2014 Daniel Steinhauer                                        *
 *                                                                   *
 * This file is part of QFractal.                                    *
 *********************************************************************
 * This file contains a basement class for all color themes and      *
 * several color themes derived from it.                             *
 *********************************************************************/

#ifndef COLORSET_H
#define COLORSET_H

#include <QColor>

/*
 * The basement for all color themes.
 */
class ColorSet
{
public:
  ColorSet ();
  
  /* 
   * Make the destructor virtual, because we
   * don't know, which class to destroy before run time.
   */
  virtual ~ColorSet ();
  
  //Get the color for specific nr. of iterations.
  virtual QColor getColor (const unsigned int) = 0;
};


/*
 * This color theme just uses six colors rotational.
 */
class SimpleColor : public ColorSet
{
private:
  static const QColor basic_set[6];
  
public:
  SimpleColor ();
  ~SimpleColor ();
  
  QColor getColor (const unsigned int);
};


/*
 * This color theme uses colors fading from black
 * to endcolor.
 * But it uses black for max iterations.
 */
class FadingLightning : public ColorSet
{
private:
  QColor endcolor;

public:
  FadingLightning (const QColor);
  ~FadingLightning ();
  
  QColor getColor (const unsigned int);
};


/*
 * This color theme uses a linear gradient from col1 to col2.
 */
class GradientColor : public ColorSet
{
private:
  QColor col1, col2;
  
public:
  GradientColor (const QColor, const QColor);
  ~GradientColor ();
  
  QColor getColor (const unsigned int);
};


/*
 * This color theme loads colors from a textfile (a map)
 * and uses them rotational.
 * The map file looks like this:
 *   #Red #Green #Blue
 *   128 255 0
 *   255 255 0
 *   ...
 */
class Filemap : public ColorSet
{
private:
  QColor *map;
  unsigned int mapSize;
  
public:
  Filemap (const char*);
  ~Filemap ();
  
  QColor getColor (const unsigned int);
};


/*
 * This color theme uses all colors of the rainbow by
 * setting red, green and blue in a circle and rotating
 * it.
 */
class RainbowColors : public ColorSet
{
private:
  static const double phi_red, phi_green, phi_blue;
public:
  RainbowColors ();
  ~RainbowColors ();
  
  QColor getColor (const unsigned int);
};

#endif