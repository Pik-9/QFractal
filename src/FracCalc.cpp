/************************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.              *
 *                                                                      *
 * (C) 2021 Daniel Steinhauer <d.steinhauer@mailbox.org>                *
 *                                                                      *
 * QFractal is free software; you can redistribute it and/or modify     *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * QFractal is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with QFractal; if not, write to the Free Software Foundation,  *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA    *
 ************************************************************************/

/**
 * @file This file contains the function definitions of the calculation
 * thread.
 */

#include <omp.h>

#include "FracCalc.hpp"

Calculator::Calculator ()
  : QThread ()
{}

void Calculator::setValues (const double l_, const double b_, const double w_,
                        const double h_, const unsigned int xpixels,
                        const unsigned int ypixels, CFractal* fr)
{
  leftBorder = l_;
  bottomBorder = b_;
  rWidth = w_;
  rHeight = h_;
  xPix = xpixels;
  yPix = ypixels;
  
  //If we already have a fractal, remove it from memory properly before.
  if (!frac)  {
    delete frac;
  }
  frac = fr;
}

void Calculator::run ()
{
  double xSteps = rWidth / (double) xPix;
  double ySteps = rHeight / (double) yPix;
  unsigned int *points = new unsigned int[xPix * yPix];

  #pragma omp parallel for
  for (unsigned int iy = 0; iy < yPix; ++iy)  {
    for (unsigned int ix = 0; ix < xPix; ++ix)  {
      points[xPix * iy + ix] = frac->calcIter (leftBorder + xSteps * ix, bottomBorder + ySteps * iy);
    }
  }
  
  emit giveMatrix (points);
}
