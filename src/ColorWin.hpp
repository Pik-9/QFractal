/*********************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.           *
 *                                                                   *
 * (C) 2014 Daniel Steinhauer                                        *
 *                                                                   *
 * This file is part of QFractal.                                    *
 *********************************************************************
 * This file contains the window to choose the color theme and       *
 * its function declarations.                                        *
 *********************************************************************/

#ifndef COLORWIN_H
#define COLORWIN_H

#include <QRadioButton>
#include <QHBoxLayout>
#include <QStackedLayout>
#include <QGroupBox>
#include <QGridLayout>
#include <QListWidget>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QPaintEvent>
#include <QColorDialog>
#include <QFileDialog>

#include "CenteredWidget.hpp"
#include "FracWidget.hpp"

/*
 * This is just a button which shows a selected color.
 */
class ColorButton : public QPushButton
{
  Q_OBJECT
private:
  QColor color;
  QColorDialog *chooser;
  
public:
  ColorButton ();
  QColor getColor ();
  void paintEvent (QPaintEvent*);
  
public slots:
  void setColor (QColor);
};


/*
 * The widget to select the color theme
 */
class ColorWindow : public CenteredWidget
{
  Q_OBJECT
private:
  //The canvas to set the color theme for
  FracCanvas *canvas;
  
  QGridLayout *lmain;
  
  QListWidget *cslist;
  QStackedLayout *csdias;
  QPushButton *bok, *bcan;
  
  //Page for Simple Layout
  QGroupBox *gsimple;
  QHBoxLayout *lsimple;
  QLabel *simple_label;
  
  //Page for Rainbow Layout
  QGroupBox *grainbow;
  QHBoxLayout *lrainbow;
  QLabel *rainbow_label;
  
  //Page for Fading Layout
  QGroupBox *gfading;
  QGridLayout *lfading;
  QLabel *fading_label;
  ColorButton *fading_button;
  
  //Page for Gradient Layout
  QGroupBox *ggrad;
  QGridLayout *lgrad;
  QLabel *lcol1, *lcol2;
  ColorButton *bcol1, *bcol2;
  
  //Page for File Layout
  QGroupBox *gfile;
  QGridLayout *lfile;
  QLabel *cfile;
  QLineEdit *tfile;
  QPushButton *bfile;
  QFileDialog *file_dia;
  
public:
  ColorWindow (FracCanvas*);
  
public slots:
  void clickOk ();
};

#endif
