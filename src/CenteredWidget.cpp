/************************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.              *
 *                                                                      *
 * (C) 2021 Daniel Steinhauer <d.steinhauer@mailbox.org>                *
 *                                                                      *
 * QFractal is free software; you can redistribute it and/or modify     *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * QFractal is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with QFractal; if not, write to the Free Software Foundation,  *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA    *
 ************************************************************************/

/**
 * @file This file contains a basic centered dialog window.
 */

#include "CenteredWidget.hpp"

#include <QGuiApplication>
#include <QStyle>
#include <QScreen>

CenteredWidget::CenteredWidget (const unsigned int width, const unsigned int height)
  : QWidget ()
{
  setWindowModality (Qt::ApplicationModal);
  setFixedSize (width, height);

  // Move window to screen center
  QScreen *screen = QGuiApplication::primaryScreen ();
  this->setGeometry (
    QStyle::alignedRect (
      Qt::LeftToRight,
      Qt::AlignCenter,
      this->size (),
      screen->availableGeometry ()
    )
  );
}

CenteredWidget::~CenteredWidget ()
{
}
