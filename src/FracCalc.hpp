/*********************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.           *
 *                                                                   *
 * (C) 2014 Daniel Steinhauer                                        *
 *                                                                   *
 * This file is part of QFractal.                                    *
 *********************************************************************
 * This file contains the thread that does the calculation of the    *
 * fractals and returns a matrix of uint with the iterations.        *
 *********************************************************************/

#ifndef FRACCALC_H
#define FRACCALC_H

#include <QThread>

#include "Fractal.hpp"

/*
 * To keep the main window usable during calculation
 * outsource it to a separate thread.
 */
class Calculator : public QThread
{
  Q_OBJECT

private:
  double leftBorder, bottomBorder, rWidth, rHeight;
  unsigned int xPix, yPix;
  CFractal *frac;
  
public:
  Calculator ();
  
  //Surrender the values for detail of fractal
  void setValues (const double,
                  const double,
                  const double,
                  const double,
                  const unsigned int,
                  const unsigned int,
                  CFractal*);
  void run ();

signals:
  //After finishing the calculation return fractal as matrix
  void giveMatrix (unsigned int* matrix);
};

#endif