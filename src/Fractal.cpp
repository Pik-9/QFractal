/************************************************************************
 * QFractal - A Mandelbrot and Julia fractal draw program.              *
 *                                                                      *
 * (C) 2021 Daniel Steinhauer <d.steinhauer@mailbox.org>                *
 *                                                                      *
 * QFractal is free software; you can redistribute it and/or modify     *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * QFractal is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with QFractal; if not, write to the Free Software Foundation,  *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA    *
 ************************************************************************/

 /**
 * @file This file contains the function definitions for the fractal
 * calculations.
 */

#include "Fractal.hpp"

#include <math.h>

unsigned int CFractal::max_iter = 16;

CFractal::CFractal ()
{}

CFractal::~CFractal ()
{}

CMandelbrot::CMandelbrot ()
{}

CMandelbrot::~CMandelbrot ()
{}

unsigned int CMandelbrot::calcIter (const double pos_x, const double pos_y)
{
  double c_x = pos_x;
  double c_y = pos_y;
  double z_x = 0.0;
  double z_y = 0.0;
  
  unsigned int n;
  double zx_, zy_;
  
  //Repeat until radius >= 2 or max iterations reached.
  for (n = 0; n < max_iter; ++n)  {
    zx_ = z_x * z_x - z_y * z_y + c_x;
    zy_ = 2 * z_x * z_y + c_y;
    z_x = zx_;
    z_y = zy_;
    if (sqrt (z_x * z_x + z_y * z_y) >= 2.0) {
      break;
    }
  }
  return n;
}

CJulia::CJulia (const double pos_x, const double pos_y)
  : c_x (pos_x), c_y (pos_y)
{}

CJulia::~CJulia ()
{}

unsigned int CJulia::calcIter (const double pos_x, const double pos_y)
{
  double z_x = pos_x;
  double z_y = pos_y;
  
  unsigned int n;
  double zx_, zy_;
  
  //Repeat until radius >= 2 or max iterations reached.
  for (n = 0; n < max_iter; ++n)  {
    zx_ = z_x * z_x - z_y * z_y + c_x;
    zy_ = 2 * z_x * z_y + c_y;
    z_x = zx_;
    z_y = zy_;
    if (sqrt (z_x * z_x + z_y * z_y) >= 2.0) {
      break;
    }
  }
  return n;
}
